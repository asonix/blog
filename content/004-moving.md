+++
title = "Moving"
description = "asonix.dog services will be offline"
date = 2024-07-20
[taxonomies]
tags = ["life", "admin"]
+++

I will be moving in two weeks.

Because of this, all asonix.dog services will be OFFLINE for somewhere between a few hours and a few
days starting Friday August 2nd at 1pm UTC.

If you need to access source code for anything I work on, the important stuff is mirrored on
codeberg: [https://codeberg.org/asonix](https://codeberg.org/asonix).

I will do my best to bring everything back online in a timely manner but we'll need to see how it
actually goes when we get there.
